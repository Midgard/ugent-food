#!/usr/bin/env python3

import json
import datetime
import sys
import urllib.request
from urllib.error import HTTPError
from os import path


vegetarian = path.exists(path.expanduser("~/.config/ugent-food/vegetarian"))


def print_for(d):
	# Fetch from API
	try:
		menu = json.loads(urllib.request.urlopen(
			"http://zeus.ugent.be/hydra/api/2.0/resto/menu/nl/{}/{}/{}.json".format(d.year, d.month, d.day)).read().decode(
			'utf-8'))
		# Print menu
		print(f"╤ \033[1m{d:%Y-%m-%d %A}\033[0m")

		print("├ Soep")
		for s in menu["meals"]:
			if s["kind"] == "soup":
				print("│ * {}".format(s["name"]))

		print("│")
		print("├ Hoofdgerechten")
		for m in menu["meals"]:
			if m["kind"] in ("soup",):
				continue
			elif m["kind"] == "meat":
				kind = "Vlees"
				if vegetarian:
					continue
			elif m["kind"] == "fish":
				kind = "Vis"
				if vegetarian:
					continue
			elif m["kind"] == "vegetarian":
				kind = "Vegetarisch"
			elif m["kind"] == "vegan":
				kind = "Vegan"
			else:
				kind = m["kind"]

			print("│ * {0}: {price} {name}".format(kind, **m))

		if menu["vegetables"]:
			print("├ Groenten")
			for v in menu["vegetables"]:
				print("| * {}".format(v))
	except HTTPError:
		print("│ Restaurant gesloten")
	print("┴")


# What day
weekdagen = ('ma', 'di', 'wo', 'do', 'vr')
deltas = {
	'morgen': 1,
	'overmorgen': 2,
	'overovermorgen': 3,
	'overoverovermorgen': 4,
	'overoveroverovermorgen': 5,
	'overoveroveroverovermorgen': 6,
	'volgende': 7
}

d = datetime.date.today()

# Remove process name from arg list
sys.argv.pop(0)

if sys.argv:
	while sys.argv and sys.argv[0] in deltas:
		d += datetime.timedelta(deltas[sys.argv[0]])
		sys.argv.pop(0)

	if sys.argv and sys.argv[0][0:2] in weekdagen:
		while d.weekday() != weekdagen.index(sys.argv[0][0:2]):
			d += datetime.timedelta(1)

	print_for(d)
else:
	def d_plus(days):
		return d + datetime.timedelta(days=days)

	print_for(d)
	print()
	print_for(d_plus(1))
	print()
	print_for(d_plus(2))
	print()
	print_for(d_plus(3))
